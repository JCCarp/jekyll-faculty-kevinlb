---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

I'm a member of the Computer Science Department at the University of British Columbia, an associate member of the Vancouver School of Economics., and an associate faculty member at the Alberta Machine Intelligence Institute (Amii). I'm Director of the UBC ICICS Centre for Artificial Intelligence Decision-making and Action (CAIDA). I am affiliated with the Institute for Computing, Information & Cognitive Systems (ICICS), the Pacific Institute for the Mathematical Sciences (PIMS), the Laboratory for Computational Intelligence (LCI) and the Algorithms Lab. I run the Game Theory and Decision Theory Seminar (GT-DT). I'm a faculty associate at the Peter Wall Institute for Advanced Studies.


## Research Interests

I am enthusiastic about research; it's one of my favourite ways to spend my time. I particularly enjoy trying to frame existing problems in new ways (e.g., more than trying to solve open problems specified by others), and am most satisfied when a single project can run the whole spectrum from theoretical rigor to practical realization as working software. My desire to craft new problem formulations draws me to interdisciplinary approaches; some of my favourite papers draw together theoretical tools from different disciplines. I favour collaborations, and hence have worked widely with colleagues and both graduate and undergraduate students. (Thus, nearly all the work described below involves collaborators; when discussing specific papers I use the first person plural to emphasize their contributions. Please see my publications page for details.) Overall, I have coauthored over 100 peer-reviewed technical articles, five book chapters and two books. One of these books has become a standard text in its area; two years after publication, it is now in its second printing, and the free online PDF has been downloaded by over 50,000 people from over 150 countries.

I conduct research in two distinct areas: algorithmic game theory and empirical algorithmics and machine learning.

## Selected Publications

{% bibliography --file selected.bib %}
